class Friend {
    private Mediator mediator;
    private String id;

    public Friend(String id) { this.id = id; }
    public void registerMediator(Mediator mediator) { this.mediator = mediator; }
    public String getId() { return id; }

    public void send(String id, String wiadomość) {
        System.out.println("Przesyłanie wiadomości od "+this.id+" do "+id+": "+wiadomość);
        mediator.send(id, wiadomość); // Rzeczywista komunikacja odbywa się za pośrednictwem mediatora!!!
    }

    public void getMessage(String wiadomość) {
        System.out.println("Wiadomość odebrana przez "+id+": "+wiadomość);
    }
}