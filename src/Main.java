public class Main {

    public static void main(String[] args) {
        Friend rene = new Friend("rene");
        Friend toni = new Friend("toni");
        Friend kim = new Friend("kim");

        RealMediator m = new RealMediator();
        m.registerFriend(rene);
        m.registerFriend(toni);
        m.registerFriend(kim);

        kim.send("toni", "Hello world.");
        rene.send("kim", "Witaj!");
    }
}
