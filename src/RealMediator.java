import java.util.HashMap;

class RealMediator implements Mediator {
    private HashMap<String, Friend> friends = new HashMap<String, Friend>();

    public void registerFriend(Friend k) {
        k.registerMediator(this);
        friends.put(k.getId(), k);
    }

    public void send(String id, String wiadomość) {
        friends.get(id).getMessage(wiadomość);
    }
}