##Mediator (wzorzec projektowy)
**Mediator** – wzorzec projektowy należący do grupy wzorców czynnościowych.
 Mediator zapewnia jednolity interfejs do różnych elementów danego podsystemu.

Wzorzec mediatora umożliwia zmniejszenie liczby powiązań między różnymi klasami,
 poprzez utworzenie mediatora będącego jedyną klasą, która dokładnie zna metody wszystkich innych klas, 
 którymi zarządza. Nie muszą one nic o sobie wiedzieć, jedynie przekazują polecenia mediatorowi,
  a ten rozsyła je do odpowiednich obiektów.

##Przykład zastosowania
Przykładem zastosowania będzie chat, np gdyby osoby mielibyśmy podzielić w kodzie na klasy według
 każdego użytkownika chatu i dla każdego z nich mielibyśmy przypisywać zależności to strasznie poplątane
  byłyby zależności między nimi, i w tym przykładzie dobrze jest użyć mediatora, jeśli jakaś osoba wysyła
   wiadomość to jest ona przekazywana do klasy mediatora i następnie przekazywana do odpowiedniej osoby,
    która miała dostać wiadomość. I wszystkie zależności mamy w jednej klasie, pięknie proste, czytelne i skuteczne.

##Struktura wzorca
 ![alt text](http://devman.pl/img/Mediator__1.png)
 
 Widać na nim, że klasy porozumiewają się poprzez klasę Mediator, jednak zwykle szczegółowa 
 implementacja wzorca Mediator wygląda tak jak na diagramie poniżej:
 ![alt text](http://devman.pl/img/Mediator___1.png)

##Konsekwencje stosowania
**Zalety**

1. Luźne zależności między obiektami w systemie.
2. Zależności między obiektami są elastyczne, możliwe jest łatwe rozbudowywanie zależności
3. Ponieważ cała logika jest zahermetyzowana w klasie mediatora,
    jeśli potrzebujemy dodać zależność do jakiejś klasy potrzebujemy jedynie rozszerzyć klasę mediatora.
4. Uproszczona komunikacja między klasami, ponieważ jeśli jakaś klasa chce się skomunikować z jakimiś innymi 
    klasami musi jedynie wysłać polecenie do klasy mediatora.
    
**Wady**

1. Skomplikowanie klasy Mediatora, jeśli będzie w niej za dużo zależności,
które będą odpowiedzialne za wszystko, podobnie to działa na zasadzie pierwszej zasady SOLID czyli,
jeśli w klasie Mediatora są zależności odpowiedzialne np tylko za komunikację to lepiej żeby nie było
w tej klasie zależności odpowiedzialnych za coś innego, ponieważ wtedy klasy Mediatora będą bardzo skomplikowane.

Źródło [Wikipedia](https://pl.wikipedia.org/wiki/Mediator_(wzorzec_projektowy)) oraz [Devman](http://devman.pl/pl/techniki/wzorce-projektowe-mediatormediator)